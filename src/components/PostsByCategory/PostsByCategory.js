import React from "react";
import { Box, Grid, Typography, makeStyles } from "@material-ui/core";
import { Link as LinkR } from "react-router-dom";
import Post from "./../../components/Post/Post";

const useStyles = makeStyles(theme => ({
  container: {
    minHeight: "100vh"
  },
  postsTitleBox: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3)
  },
  postTitleSubtitle: {
    marginLeft: theme.spacing(1),
    textDecoration: "none"
  }
}));

export default props => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Box
          className={classes.postTitleBox}
          display="flex"
          justifyContent="center"
          alignItems="flex-end"
        >
          <Typography variant="h4" align="center" fontWeight="fontWeightBold">
            {props.postsByCategory.data[0].category.name}
          </Typography>
          <Typography
            className={classes.postTitleSubtitle}
            variant="subtitle1"
            component={LinkR}
            to={`/all-posts/`}
            color="primary"
          >
            More ...
          </Typography>
        </Box>
      </Grid>
      {props.postsByCategory.data.slice(0, 2).map(post => (
        <Grid key={post.id} item xs={12} sm={6} md={6} lg={6}>
          <Post post={post} />
        </Grid>
      ))}
      {props.postsByCategory.data.slice(2).map(post => (
        <Grid key={post.id} item xs={12} sm={6} md={4}>
          <Post post={post} />
        </Grid>
      ))}
    </Grid>
  );
};
