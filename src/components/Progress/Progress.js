import React from "react";
import { makeStyles, CircularProgress } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    "& > * + *": {
      marginLeft: theme.spacing(2)
    },
    justifyContent: "center",
    margin: "auto",
    position: "absolute",
    alignItems: "center",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
}));

export default () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CircularProgress color="primary" />
    </div>
  );
};
