import React from "react";
import { makeStyles, Grid, Typography } from "@material-ui/core";
import heroImg from "./../../assets/images/6.jpg";
import heroImgMobile from "./../../assets/images/6-mobile.jpg";
import Newsletter from "./../NewsLetter/NewsLetter";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-start",
    marginTop: theme.spacing(7),
    background: `url(${heroImgMobile}) no-repeat`,
    backgroundPosition: "center center",
    height: 500,
    backgroundSize: "cover",
    [theme.breakpoints.up("lg")]: {
      alignItems: "center",
      justifyContent: "flex-start",
      backgroundPosition: "90% 50%",
      background: `url(${heroImg}) no-repeat`
    }
  },
  heroBox: {
    textAlign: "center"
  },
  heroTitle: {
    width: "100%",
    fontWeight: "bold",
    fontSize: "2rem",
    lineHeight: 1,
    [theme.breakpoints.up("lg")]: {
      textAlign: "left",
      fontSize: "2.5rem"
    }
  },
  heroSubtitle: {
    fontWeight: "bold",
    fontSize: "1.5rem",
    marginTop: theme.spacing(2),
    [theme.breakpoints.up("lg")]: {
      textAlign: "left"
    }
  },
  heroDescription: {
    display: "none",
    textAlign: "center",
    marginTop: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "block"
    },
    [theme.breakpoints.up("lg")]: {
      textAlign: "left"
    }
  }
}));

export default props => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid className={classes.heroBox} item xs={12} lg={6}>
          <Typography className={classes.heroTitle}>
            {props.titleFirst}
          </Typography>
          <Typography className={classes.heroTitle}>
            {props.titleSecond}
          </Typography>
          <Typography className={classes.heroSubtitle} color="primary">
            {props.subtitle}
          </Typography>
          <Typography className={classes.heroDescription} variant="h6">
            {props.description}
          </Typography>
          <Newsletter />
        </Grid>
      </Grid>
    </div>
  );
};
