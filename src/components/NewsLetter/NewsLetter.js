import React, { useState } from "react";
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  makeStyles
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  containerButton: {
    textAlign: "center",
    [theme.breakpoints.up("lg")]: {
      textAlign: "left"
    }
  },
  heroButton: {
    marginTop: theme.spacing(2)
  }
}));
export default () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [inputVal, setInputVal] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubscribe = e => {
    e.preventDefault();
    setInputVal("");
    setOpen(false);
  };

  return (
    <div className={classes.containerButton}>
      <Button
        className={classes.heroButton}
        variant="contained"
        color="primary"
        onClick={handleClickOpen}
      >
        Join the newsletter
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form onSubmit={handleSubscribe}>
          <DialogTitle id="form-dialog-title" align="center">
            Subscribe to the free newsletter
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              To subscribe to this website, please enter your email address
              here. We will send updates occasionally.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Email Address"
              type="email"
              fullWidth
              value={inputVal}
              onChange={e => setInputVal(e.target.value)}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              No thanks
            </Button>
            <Button color="primary" type="submit">
              Subscribe
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};
