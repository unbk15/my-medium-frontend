import React from "react";
import moment from "moment";
import { Link as LinkR } from "react-router-dom";
import {
  Avatar,
  Button,
  Box,
  CardActions,
  CardContent,
  CardMedia,
  Card,
  Typography,
  makeStyles
} from "@material-ui/core";
import { indigo, teal, purple, red } from "@material-ui/core/colors";

const useStyles = makeStyles(theme => ({
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  postTitle: {
    textDecoration: "none",
    color: "inherit",
    fontSize: "1.25rem",
    fontWeight: "bold",
    marginBottom: theme.spacing(0),
    marginLeft: theme.spacing(1)
  },
  postContent: {
    paddingBottom: theme.spacing(0)
  },
  postDescription: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2)
  }
}));

export default props => {
  const classes = useStyles();

  const color = () => {
    switch (props.post.color) {
      case "blue":
        return indigo[600];
      case "green":
        return teal[600];
      case "red":
        return red[600];
      case "purple":
        return purple[600];
      default:
        return indigo[600];
    }
  };

  return (
    <Card>
      <CardMedia
        className={classes.media}
        image={props.post.img}
        src={props.img}
        title={props.post.title}
      />
      <CardContent className={classes.postContent}>
        <Box display="flex" alignItems="center">
          <Avatar style={{ backgroundColor: color(), opacity: 0.5 }}>
            {props.post.category.charAt(0)}
          </Avatar>
          <Typography
            className={classes.postTitle}
            gutterBottom
            variant="body1"
            component={LinkR}
            to={`/post-details/${props.post.id}/${props.post.categoryId}/${props.post.category}`}
          >
            {props.post.title.slice(0, 20)}
          </Typography>
        </Box>
        <Typography className={classes.postDescription} variant="body1">
          {props.post.description.slice(0, 80)} ...
        </Typography>
        <Typography variant="subtitle2">
          {moment(props.post.createdAt).format("MMMM Do YYYY,h:mm:ss a")}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Button
          size="small"
          color="primary"
          component={LinkR}
          to={`/post-details/${props.post.id}/${props.post.categoryId}/${props.post.category}`}
        >
          Read More
        </Button>
      </CardActions>
    </Card>
  );
};
