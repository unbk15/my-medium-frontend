import React, { useState } from "react";
import { Link as LinkR } from "react-router-dom";
import {
  AppBar,
  BottomNavigation,
  BottomNavigationAction,
  Container,
  makeStyles
} from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import InfoIcon from "@material-ui/icons/Info";
import DescriptionIcon from "@material-ui/icons/Description";
import EmailIcon from "@material-ui/icons/Email";
import { indigo } from "@material-ui/core/colors";

const useStyles = makeStyles({
  footerBar: {
    backgroundColor: "transparent",
    position: "relative",
    minHeight: "6vh"
  },
  buttonClass: {
    color: "inherit"
  },
  activeClass: {
    color: indigo[600]
  }
});

export default props => {
  const classes = useStyles();
  const [value, setValue] = useState("");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <AppBar className={classes.footerBar}>
      <Container maxWidth="md">
        <BottomNavigation
          value={value}
          onChange={handleChange}
          className={classes.root}
        >
          <BottomNavigationAction
            component={LinkR}
            to="/"
            label="Home"
            value="home"
            icon={<HomeIcon />}
          />
          <BottomNavigationAction
            component={LinkR}
            to="/about"
            label="About"
            value="about"
            icon={<InfoIcon />}
          />
          <BottomNavigationAction
            component={LinkR}
            to="/posts"
            label="Posts"
            value="posts"
            icon={<DescriptionIcon />}
          />
          <BottomNavigationAction
            component={LinkR}
            label="Contact"
            value="contact"
            to="/contact"
            icon={<EmailIcon />}
          />
        </BottomNavigation>
      </Container>
    </AppBar>
  );
};
