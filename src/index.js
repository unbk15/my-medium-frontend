import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import firstReducer from "./store/reducers/firstReducer";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";

import Menu from "./components/Menu/Menu";
import Footer from "./components/Footer/Footer";

// Routing Containers

import AddCategory from "./containers/AddCategory/AddCategory";
import AddPost from "./containers/AddPost/AddPost";
import About from "./containers/About/About";
import Contact from "./containers/Contact/Contact";
import PostDetails from "./containers/PostDetails/PostDetails";
import Home from "./containers/Home/Home";
import Notfound from "./components/NotFound/notFound";
import Posts from "./containers/Posts/Posts";
import SignUp from "./containers/SignUp/SignUp";
import SignIn from "./containers/SignIn/SignIn";
import Dashbord from "./containers/Dashbord/Dashbord";

const store = createStore(firstReducer);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Menu />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/add-category" exact component={AddCategory} />
        <Route path="/add-post" exact component={AddPost} />
        <Route
          path="/post-details/:id/:categoryId/:category"
          exact
          component={PostDetails}
        />
        <Route path="/about" exact component={About} />
        <Route path="/posts" exact component={Posts} />
        <Route path="/contact" exact component={Contact} />
        <Route path="/signup" exact component={SignUp} />
        <Route path="/signin" exact component={SignIn} />
        <Route path="/dashbord" exact component={Dashbord} />
        <Route component={Notfound} />
      </Switch>
      <Footer />
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
