import React, { useState, useEffect } from "react";
import axios from "axios";
import Post from "../../components/Post/Post";
import Progress from "../../components/Progress/Progress";
import {
  Box,
  Container,
  Grid,
  FormControl,
  Typography,
  makeStyles,
  MenuItem,
  Select
} from "@material-ui/core";
import { arrayBufferToBase64, convertDataToObject } from "./../../functions";

const useStyles = makeStyles(theme => ({
  container: {
    minHeight: "100vh",
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5)
  },
  titleContainer: {
    marginBottom: theme.spacing(5)
  },
  postsTitle: {
    fontWeight: "bold"
  },
  button: {
    display: "block",
    marginTop: theme.spacing(2)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 80
  }
}));

export default () => {
  const classes = useStyles();
  const [categoryType, setCategoryType] = useState("");
  const [categories, setCatgories] = useState([]);
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8000/api/v1/category/all-categories`)
      .then(res => setCatgories(res.data))
      .catch(err => console.log(err));
  }, []);
  useEffect(() => {
    if (categoryType === "") {
      axios
        .get(`http://localhost:8000/api/v1/post/all-posts?&order=desc`)
        .then(postsData => {
          let posts = postsData.data.map(post => {
            // converting Buffer Img to Htm readable Img
            let base64Flag = "data:image/jpeg;base64,";
            let imgStr = arrayBufferToBase64(post.photo.data.data);
            let img = base64Flag + imgStr;
            return convertDataToObject(post, img);
          });
          setPosts(posts);
        });
    } else {
      axios
        .get(
          `http://localhost:8000/api/v1/post/all-posts-by-category?category=${categoryType}&order=desc`
        )
        .then(postsData => {
          let posts = postsData.data.map(post => {
            // converting Buffer Img to Htm readable Img
            let base64Flag = "data:image/jpeg;base64,";
            let imgStr = arrayBufferToBase64(post.photo.data.data);
            let img = base64Flag + imgStr;
            return convertDataToObject(post, img);
          });
          setPosts(posts);
        });
    }
  }, [categoryType]);

  const handleChange = event => {
    setCategoryType(event.target.value);
  };

  return (
    <Container className={classes.container} maxWidth="md">
      <Box
        className={classes.titleContainer}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography className={classes.postsTitle} variant="h4">
          All Posts
        </Typography>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Typography variant="h6">Filter: </Typography>

          <FormControl className={classes.formControl}>
            <Select value={categoryType} onChange={handleChange} displayEmpty>
              <MenuItem value="">None</MenuItem>
              {categories.length > 0
                ? categories.map(category => {
                    return (
                      <MenuItem key={category._id} value={category._id}>
                        {category.name}
                      </MenuItem>
                    );
                  })
                : null}
            </Select>
          </FormControl>
        </Box>
      </Box>
      <Grid container spacing={3}>
        {posts.length > 0 ? (
          posts.map(post => (
            <Grid key={post.id} item xs={12} sm={6} md={4}>
              <Post post={post} />
            </Grid>
          ))
        ) : (
          <Progress />
        )}
      </Grid>
    </Container>
  );
};
