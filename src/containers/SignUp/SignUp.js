import React, { useState } from "react";
import axios from "axios";
import {
  Avatar,
  Button,
  Container,
  CssBaseline,
  Grid,
  Link,
  makeStyles,
  TextField,
  Typography
} from "@material-ui/core";
import { Link as LinkR } from "react-router-dom";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import AlertConfirm from "./../../components/AlertConfirm/AlertConfirm";
import AlertError from "./../../components/AlertError/AlertError";

const useStyles = makeStyles(theme => ({
  container: {
    minHeight: "85vh"
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default () => {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [successSnack, setSuccessSnack] = useState(false);
  const [errorSnack, setErrorSnack] = useState(false);

  const submitFormHandler = e => {
    e.preventDefault();
    axios
      .post(`http://localhost:8000/api/v1/signup`, {
        email,
        password,
        passwordConfirm
      })
      .then(res => setSuccessSnack(true))
      .catch(err => setErrorSnack(true));
    setEmail("");
    setPassword("");
    setPasswordConfirm("");
  };

  return (
    <Container className={classes.container} component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={submitFormHandler}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirmPassword"
                label="Confirm Password"
                type="password"
                id="confirm-password"
                value={passwordConfirm}
                onChange={e => setPasswordConfirm(e.target.value)}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <LinkR to="/signin">
                <Link href="" variant="body2">
                  Already have an account? Sign in
                </Link>
              </LinkR>
            </Grid>
          </Grid>
        </form>
      </div>
      <AlertConfirm
        openSuccessSnack={successSnack}
        sendMessageSnack="The account was created successfully"
        closeSucccessSnack={() => setSuccessSnack(false)}
      />
      <AlertError
        openErrorSnack={errorSnack}
        sendMessageSnack="There was an error creating the account"
        closeErrorSnack={() => setErrorSnack(false)}
      />
    </Container>
  );
};
