import React, { useState } from "react";
import {
  makeStyles,
  Container,
  Typography,
  TextField,
  Button
} from "@material-ui/core";
import axios from "axios";
import red from "@material-ui/core/colors/red";
import MenuBackend from "./../../backend/Menu/Menu";

const redColor = red[500]; // #F44336

const useStyles = makeStyles(theme => ({
  container: {
    minHeight: "80vh",
    marginTop: theme.spacing(3)
  },
  heading: {
    textAlign: "center"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginBottom: 50,
    width: 400
  },
  containerForm: {
    marginLeft: "auto",
    marginRight: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  submitButton: {
    textAlign: "center"
  }
}));

export default function BasicTextFields() {
  const classes = useStyles();
  let [category, setCategory] = useState("");
  let [color, setColor] = useState("");
  let [error, setError] = useState(null);

  const handleSubmit = e => {
    e.preventDefault();
    axios
      .post(`http://localhost:8000/api/v1/category/create`, {
        name: category,
        color: color
      })
      .then(res => console.log(res))
      .catch(err => {
        setError(err.response.data.error);
      });
    setCategory("");
    setColor("");
  };

  const handleOnClick = () => {
    setError(null);
  };
  return (
    <Container maxWidth="md" className={classes.container}>
      <MenuBackend className={classes.menuBackend} />
      <Typography className={classes.heading} variant="h4">
        Adding a New Category
      </Typography>
      <form onSubmit={handleSubmit} noValidate autoComplete="off">
        <div className={classes.containerForm}>
          <TextField
            id="category"
            className={classes.textField}
            label="Add Category"
            margin="normal"
            name="category"
            onChange={e => setCategory(e.target.value)}
            onClick={handleOnClick}
            value={category}
            helperText={error ? error : ""}
            color={redColor}
          />
          <TextField
            id="color"
            className={classes.textField}
            label="Add A Color"
            margin="normal"
            name="color"
            onChange={e => setColor(e.target.value)}
            onClick={handleOnClick}
            value={color}
            helperText={error ? error : ""}
            color={redColor}
          />
          <div className={classes.submitButton}>
            <Button
              type="submit"
              color="primary"
              variant="outlined"
              size="small"
              disabled={!category || !color}
            >
              Add
            </Button>
          </div>
        </div>
      </form>
    </Container>
  );
}
