import React, { useEffect, useState, Fragment } from "react";
import { Container, Grid } from "@material-ui/core";
import { Link as LinkR } from "react-router-dom";
import axios from "axios";
import { Box, makeStyles, Typography } from "@material-ui/core";
import Progress from "../../components/Progress/Progress";
import Post from "../../components/Post/Post";
import Hero from "../../components/Hero/Hero";
import { arrayBufferToBase64, convertDataToObject } from "./../../functions";

const useStyles = makeStyles(theme => ({
  container: {
    minHeight: "100vh"
  },
  gridContainer: {
    marginTop: 0,
    marginBottom: 30
  },
  postTitleBox: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5)
  },
  latestPostsLink: {
    marginLeft: theme.spacing(1),
    textDecoration: "none",
    fontWeight: "bold"
  },
  latestPostsTitle: {
    fontWeight: "bold"
  }
}));

const Home = props => {
  const classes = useStyles();

  const [recentPostsThree, setRecentPostsThree] = useState([]);
  const [recentPostsFour, setRecentPostsFour] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:8000/api/v1/post/all-posts?limit=5&order=desc", {
        headers: { "Content-Type": "application/json" }
      })
      .then(postsData => {
        let posts = postsData.data.map(post => {
          // converting Buffer Img to Htm readable Img
          let base64Flag = "data:image/jpeg;base64,";
          let imgStr = arrayBufferToBase64(post.photo.data.data);
          let img = base64Flag + imgStr;
          return convertDataToObject(post, img);
        });
        setRecentPostsThree(posts.slice(0, 2));
        setRecentPostsFour(posts.slice(2));
      })
      .catch(err => console.log(err));
  }, []);

  return (
    <Container maxWidth="md" className={classes.container}>
      {recentPostsThree.length > 0 ? (
        <Fragment>
          <Hero
            titleFirst=" Learn to build modern"
            titleSecond="web applications"
            subtitle="using react"
          />
          <Grid className={classes.gridContainer} container spacing={3}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Box
                className={classes.postTitleBox}
                display="flex"
                justifyContent="center"
                alignItems="flex-end"
              >
                <Typography
                  variant="h4"
                  align="center"
                  className={classes.latestPostsTitle}
                >
                  Latest Posts
                </Typography>
                <Typography
                  className={classes.latestPostsLink}
                  variant="subtitle1"
                  component={LinkR}
                  to={`/posts`}
                  color="primary"
                >
                  More ...
                </Typography>
              </Box>
            </Grid>
            {recentPostsThree.map(post => (
              <Grid key={post.id} item xs={12} sm={6} md={6} lg={6}>
                <Post post={post} />
              </Grid>
            ))}
            {recentPostsFour.map(post => (
              <Grid key={post.id} item xs={12} sm={6} md={4}>
                <Post post={post} />
              </Grid>
            ))}
          </Grid>
        </Fragment>
      ) : (
        <Progress />
      )}
    </Container>
  );
};

export default Home;
