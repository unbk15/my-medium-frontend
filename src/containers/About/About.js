import React from "react";
import {
  Container,
  Box,
  Paper,
  Typography,
  makeStyles,
  Chip
} from "@material-ui/core";
import Hero from "../../components/Hero/Hero";
import imgAboutOne from "../../assets/images/cv-picture.jpg";

const useStyles = makeStyles(theme => ({
  about: {
    backgroundColor: "rgba(0, 0, 0, 0.02)",
    paddingTop: theme.spacing(5),
    paddingBottom: theme.spacing(5)
  },
  aboutBox: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    padding: theme.spacing(3)
  },
  aboutPaperImg: {
    borderRadius: "50%",
    width: 120,
    height: 120,
    float: "left",
    shapeOutside: "circle()",
    marginRight: 40
  },
  aboutChipBox: {
    display: "flex",
    justifyContent: "center",
    marginTop: theme.spacing(2),
    [theme.breakpoints.up("lg")]: {
      justifyContent: "flex-end"
    }
  },
  aboutChipBoxChip: {
    fontWeight: "bold"
  }
}));

export default () => {
  const classes = useStyles();
  return (
    <>
      <Container maxWidth="md">
        <Hero
          titleFirst="You have come to the right place!"
          description={`Lorem Ipsum is simply dummy text
                      of the printing and typesetting industry. 
                      Lorem Ipsum has been the industry's standard dummy 
                      text ever since the 1500s`}
        />
      </Container>
      <Box className={classes.about}>
        <Container maxWidth="md">
          <Paper className={classes.aboutBox}>
            <img
              className={classes.aboutPaperImg}
              alt="About Me"
              src={imgAboutOne}
            />
            <Typography className={classes.aboutDescription} variant="h6">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
              pharetra sem tristique gravida convallis. Duis iaculis viverra sem
              vel finibus. Nam viverra eleifend nisl et vestibulum. Proin rutrum
              tincidunt dolor, quis pharetra magna convallis ac. Aenean ac
              euismod odio. Nam sit amet iaculis nisl. Donec aliquet lacus sit
              amet dapibus cursus. Pellentesque bibendum, magna rhoncus varius
              laoreet, nulla nulla dignissim nibh, et pharetra lectus libero in
              lectus. Curabitur dapibus efficitur mauris at tempor. Mauris nec
              lacus sit amet enim porta fringilla sed at dolor. Nam finibus sed
              metus porta ultrices. Fusce maximus vitae sapien non dignissim.
            </Typography>
            <Box className={classes.aboutChipBox}>
              <Chip
                label="Timoce Ilie, Software Developer"
                variant="outlined"
                className={classes.aboutChipBoxChip}
              />
            </Box>
          </Paper>
        </Container>
      </Box>
    </>
  );
};
