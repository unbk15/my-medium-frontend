import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Container,
  makeStyles,
  TextField,
  MenuItem,
  Grid,
  Typography,
  Button,
  Box
} from "@material-ui/core";
import { DropzoneArea } from "material-ui-dropzone";
import AlertConfirm from "../../components/AlertConfirm/AlertConfirm";
import AlertError from "../../components/AlertError/AlertError";
import MenuBackend from "./../../backend/Menu/Menu";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  menu: {
    width: 200
  },
  headingPost: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3)
  },
  dropzoneClass: {
    backgroundColor: "green"
  },
  submitPost: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3)
  }
}));

const AddPost = () => {
  const classes = useStyles();

  const [categories, setCategories] = useState([]);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [author, setAuthor] = useState("");
  const [photo, setPhoto] = useState([]);
  const [successSnack, setSuccessSnack] = useState(false);
  const [errorSnack, setErrorSnack] = useState(false);

  useEffect(() => {
    axios
      .get(`http://localhost:8000/api/v1/category/all-categories`)
      .then(cate => {
        setCategories(cate.data);
      })
      .catch(err => console.log(err));
  }, []);

  const handleSelect = e => {
    setCategory(e.target.value);
  };

  const handleForm = e => {
    e.preventDefault();
    const config = { headers: { "Content-Type": "multipart/form-data" } };

    const postData = new FormData();
    postData.append("title", title);
    postData.append("description", description);
    postData.append("category", category);
    postData.append("author", author);
    postData.append("photo", photo);
    axios
      .post(`http://localhost:8000/api/v1/post/create`, postData, config)
      .then(res => {
        setSuccessSnack(true);
      })
      .catch(err => {
        setErrorSnack(true);
      });
    setTitle("");
    setDescription("");
    setAuthor("");
    setCategory("");
  };

  return (
    <Container maxWidth="md">
      <MenuBackend />
      <Typography className={classes.headingPost} variant="h4" align="center">
        Add a Post
      </Typography>
      <form
        className={classes.container}
        noValidate
        autoComplete="off"
        onSubmit={handleForm}
      >
        <Grid container spacing={3}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <TextField
              id="standard-basic"
              className={classes.textField}
              label="Title"
              margin="normal"
              fullWidth
              value={title}
              onChange={e => setTitle(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <TextField
              id="standard-basic"
              className={classes.textField}
              label="Description"
              margin="normal"
              fullWidth
              multiline
              rows="7"
              value={description}
              onChange={e => setDescription(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <TextField
              select
              label="Select Category"
              className={classes.textField}
              margin="none"
              fullWidth
              value={category}
              onChange={handleSelect}
            >
              {categories.map(option => (
                <MenuItem key={option._id} value={option._id}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <TextField
              id="standard-basic"
              className={classes.textField}
              label="Author"
              margin="none"
              fullWidth
              value={author}
              onChange={e => setAuthor(e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <DropzoneArea
              className={classes.dropzoneClass}
              maxFileSize={1000000}
              dropzoneText="Upload Photo"
              showFileNames={true}
              fullWidth
              acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
              onChange={e => setPhoto(e[0])}
              initialFiles={[]}
              filesLimit={1}
            />
          </Grid>
        </Grid>
        <Box
          display="flex"
          width="100%"
          justifyContent="center"
          className={classes.submitPost}
        >
          <Button
            type="submit"
            color="primary"
            variant="outlined"
            size="small"
            disabled={
              title === "" ||
              description === "" ||
              category === "" ||
              author === ""
            }
          >
            Add Post
          </Button>
        </Box>
      </form>
      <AlertConfirm
        openSuccessSnack={successSnack}
        sendMessageSnack="The post was sent succesfully"
        closeSucccessSnack={() => setSuccessSnack(false)}
      />
      <AlertError
        openErrorSnack={errorSnack}
        sendMessageSnack="There was an error sending the post"
        closeErrorSnack={() => setErrorSnack(false)}
      />
    </Container>
  );
};

export default AddPost;
