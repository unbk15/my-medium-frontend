import React from "react";
import { Link as LinkR } from "react-router-dom";
import { Container, Button, makeStyles } from "@material-ui/core";

// The use of React.forwardRef will no longer be required for react-router-dom v6.
// See https://github.com/ReactTraining/react-router/issues/6056

const useStyles = makeStyles(theme => ({
  container: {
    minHeight: "87vh",
    paddingTop: theme.spacing(5)
  }
}));

export default function ButtonRouter() {
  const classes = useStyles();
  return (
    <Container className={classes.container} maxWidth="md">
      <div>
        <LinkR to="/add-category">
          <Button color="primary">Add Category</Button>
        </LinkR>
        <br />
        <LinkR to="add-post">
          <Button color="primary">Add Post</Button>
        </LinkR>
      </div>
    </Container>
  );
}
