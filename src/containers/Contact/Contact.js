import React, { useState } from "react";
import axios from "axios";
import {
  Container,
  TextField,
  Paper,
  Typography,
  makeStyles,
  Button,
  Box
} from "@material-ui/core";
import AlertConfirm from "./../../components/AlertConfirm/AlertConfirm";
import AlertError from "./../../components/AlertError/AlertError";

const useStyles = makeStyles(theme => ({
  container: {
    minHeight: "87vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  box: {
    padding: theme.spacing(6, 6),
    width: "80%",
    marginTop: theme.spacing(2)
  },
  title: {
    textAlign: "center",
    marginBottom: theme.spacing(2),
    fontWeight: "bold",
    fontSize: "2rem",
    width: "100%",
    lineHeight: 1,
    [theme.breakpoints.up("lg")]: {
      fontSize: "2.5rem"
    }
  },
  form: {
    display: "flex",
    flexDirection: "column"
  },
  email: {
    marginTop: theme.spacing(2)
  },
  message: {
    marginTop: theme.spacing(2)
  },
  buttonBox: {
    marginTop: theme.spacing(2),
    display: "flex",
    justifyContent: "center"
  }
}));
export default () => {
  const classes = useStyles();

  const [name, setName] = useState("");
  const [sender, setSender] = useState("");
  const [message, setMessage] = useState("");

  const [successSnack, setSuccessSnack] = useState(false);
  const [errorSnack, setErrorSnack] = useState(false);

  const submitFormHandler = e => {
    e.preventDefault();
    axios
      .get(
        `http://localhost:8000/api/v1/email?name=${name}&sender=${sender}&message=${message}`
      )
      .then(() => setSuccessSnack(true))
      .catch(err => setErrorSnack(true));

    setName("");
    setSender("");
    setMessage("");
  };

  return (
    <Container className={classes.container} maxWidth="sm">
      <Paper className={classes.box}>
        <Typography className={classes.title}>Drop me a line</Typography>
        <form className={classes.form} onSubmit={submitFormHandler}>
          <TextField
            className={classes.name}
            required
            type="text"
            label="Name"
            value={name}
            onChange={e => setName(e.target.value)}
          />
          <TextField
            className={classes.email}
            required
            type="email"
            label="Email"
            value={sender}
            onChange={e => setSender(e.target.value)}
          />
          <TextField
            className={classes.message}
            label="Message"
            margin="normal"
            fullWidth
            multiline
            rows="5"
            value={message}
            onChange={e => setMessage(e.target.value)}
          />
          <Box className={classes.buttonBox}>
            <Button variant="outlined" color="primary" type="submit">
              Send
            </Button>
          </Box>
        </form>
      </Paper>
      <AlertConfirm
        openSuccessSnack={successSnack}
        sendMessageSnack="The message was sent succesfully"
        closeSucccessSnack={() => setSuccessSnack(false)}
      />
      <AlertError
        openErrorSnack={errorSnack}
        sendMessageSnack="There was an error sending the message"
        closeErrorSnack={() => setErrorSnack(false)}
      />
    </Container>
  );
};
