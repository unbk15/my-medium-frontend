import React, { useEffect, useState, Fragment } from "react";
import {
  Box,
  Container,
  Grid,
  makeStyles,
  Typography
} from "@material-ui/core";
import axios from "axios";
import moment from "moment";
import Progress from "./../../components/Progress/Progress";
import Post from "./../../components/Post/Post";
import { arrayBufferToBase64, convertDataToObject } from "./../../functions";

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(5),
    position: "relative",
    minHeight: "100vh"
  },
  title: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(3),
    fontWeight: "bold"
  },
  img: {
    marginTop: theme.spacing(2),
    width: "100%"
  },
  descriptionStart: {
    marginTop: theme.spacing(2)
  },
  description: {
    marginBottom: theme.spacing(3)
  },
  posts: {
    paddingTop: theme.spacing(5),
    paddingBottom: theme.spacing(5),
    backgroundColor: "rgba(0, 0, 0, 0.02)"
  },
  postsHeading: {
    color: "inherit",
    fontSize: "1.5rem",
    fontWeight: "bold",
    marginBottom: theme.spacing(3)
  }
}));

export default props => {
  const classes = useStyles();
  const id = props.match.params.id;
  const categoryId = props.match.params.categoryId;
  const category = props.match.params.category;

  const [post, setPost] = useState([]);
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    axios
      .get(`http://localhost:8000/api/v1/post/${id}`)
      .then(res => {
        let base64Flag = "data:image/jpeg;base64,";
        let imgStr = arrayBufferToBase64(res.data.photo.data.data);
        let img = base64Flag + imgStr;
        const post = {
          title: res.data.title,
          author: res.data.author,
          descriptionStart: res.data.description.slice(0, 99),
          description: res.data.description.slice(99),
          img
        };
        setPost(post);
        window.scrollTo(0, 0);
      })
      .catch(err => {
        console.log(err);
      });
  }, [id]);
  useEffect(() => {
    axios
      .get(
        `http://localhost:8000/api/v1/post/all-posts-by-category?category=${categoryId}&limit=4&order=desc`
      )
      .then(res => {
        let posts = res.data
          .filter(post => post._id !== id)
          .map(post => {
            // converting Buffer Img to Htm readable Img
            let base64Flag = "data:image/jpeg;base64,";
            let imgStr = arrayBufferToBase64(post.photo.data.data);
            let img = base64Flag + imgStr;
            return convertDataToObject(post, img);
          });
        setPosts(posts);
      })
      .catch(err => console.log(err));
  }, [categoryId, id]);
  return (
    <Fragment>
      <Container maxWidth="md" className={classes.container}>
        {post.length !== 0 ? (
          <Fragment>
            <Typography className={classes.title} variant="h4">
              {post.title}
            </Typography>
            <Typography variant="body1">{post.author}</Typography>
            <Typography variant="body2">
              {moment(post.createdAt).format("MMMM Do YYYY,h:mm:ss a")}
            </Typography>
            <img className={classes.img} src={post.img} alt="dsdsds" />
            <Box
              className={classes.descriptionStart}
              fontSize="h6.fontSize"
              fontWeight="fontWeightBold"
            >
              {post.descriptionStart}
            </Box>
            <Box className={classes.description} fontSize="h6.fontSize">
              {post.description}
            </Box>
          </Fragment>
        ) : (
          <Progress />
        )}
      </Container>
      {posts.length > 0 ? (
        <Box className={classes.posts}>
          <Container maxWidth="md" className={classes.containerPosts}>
            <Typography className={classes.postsHeading} variant="body1">
              More About {category}
            </Typography>
            <Grid container spacing={3}>
              {posts.map(post => (
                <Grid key={post.id} item xs={12} sm={6} md={4}>
                  <Post post={post} />
                </Grid>
              ))}
            </Grid>
          </Container>
        </Box>
      ) : null}
    </Fragment>
  );
};
