import React from "react";
import { Button, Menu, MenuItem, makeStyles } from "@material-ui/core";
import { Link as LinkR } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  menu: {
    marginTop: theme.spacing(7),
    textAlign: "right"
  }
}));

export default () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <div className={classes.menu}>
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          Menu
        </Button>
      </div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <LinkR to="/add-category">
          <MenuItem onClick={handleClose}>Add Category</MenuItem>
        </LinkR>
        <LinkR to="/add-post">
          <MenuItem onClick={handleClose}>Add Post</MenuItem>
        </LinkR>
      </Menu>
    </div>
  );
};
