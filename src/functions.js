// convert image from buffer to base 64

export const arrayBufferToBase64 = buffer => {
  var binary = "";
  var bytes = [].slice.call(new Uint8Array(buffer));
  bytes.forEach(b => (binary += String.fromCharCode(b)));
  return window.btoa(binary);
};
// Manipulate Data returned from the server

export const convertDataToObject = (obj, img) => {
  return {
    id: obj._id,
    title: obj.title,
    author: obj.author,
    category: obj.category.name,
    categoryId: obj.category._id,
    color: obj.category.color,
    description: obj.description,
    img,
    createdAt: obj.createdAt
  };
};
